﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Mimosa
{
    /// <summary>
    /// Interaction logic for MimosaProg.xaml
    /// </summary>
    public partial class MimosaProg : Window
    {
        public MimosaProg()
        {
            InitializeComponent();
        }

        private void imgRoos_MouseEnter(object sender, MouseEventArgs e)
        {
            Grid.Background = new SolidColorBrush(Color.FromRgb(255,0,0));
        }

        private void imgGoedenbloem_MouseEnter(object sender, MouseEventArgs e)
        {
            Grid.Background = new SolidColorBrush(Color.FromRgb(252, 173, 3));
        }

        private void imgLavender_MouseEnter(object sender, MouseEventArgs e)
        {
            Grid.Background = new SolidColorBrush(Color.FromRgb(255, 0, 255));
        }

        private void imgWilkenBitter_MouseEnter(object sender, MouseEventArgs e)
        {
            Grid.Background = new SolidColorBrush(Color.FromRgb(255, 255, 0));
        }

        private void imgRoos_MouseLeave(object sender, MouseEventArgs e)
        {
            Grid.Background = new SolidColorBrush(Color.FromRgb(224, 224, 224));
        }

        private void imgGoedenbloem_MouseLeave(object sender, MouseEventArgs e)
        {
            Grid.Background = new SolidColorBrush(Color.FromRgb(224, 224, 224));
        }

        private void imgLavender_MouseLeave(object sender, MouseEventArgs e)
        {
            Grid.Background = new SolidColorBrush(Color.FromRgb(224, 224, 224));
        }

        private void imgWilkenBitter_MouseLeave(object sender, MouseEventArgs e)
        {
            Grid.Background = new SolidColorBrush(Color.FromRgb(224, 224, 224));
        }
    }
}
